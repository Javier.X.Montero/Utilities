<?php

namespace Scinomix\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\ProcessBuilder;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class doSCIVXLPrintSplit extends Command {

    # Member Constants
    const LCLPATH      = 'C:\\cli';
    const LCLINPATH    = 'C:\\Batch Labeller\\WorkTODO';
    const LCLINSTGPATH = 'C:\\cli\\inputStaging';
    const LCLLOGPATH   = 'C:\\cli\\log';
    const UNCPATH      = '\\\\SJCISL0001\\nipsprinter';
    const UNCUSER      = 'sjcisl0001\\sjc_nipsprinter';
    const UNCPASS      = '<ADD_HERE>';
    const UNCDRVL      = 'Z';
    const UNCINDIR     = ':\\incoming';
    const UNCOUTDIR    = ':\\archive';
    const DSTEXT       = 'dst';
    const SRCEXT       = 'src';
    const CANCEL       = '***CANCEL/ABORT***';
    #const LCLAPPPATH   = "C:\\Program^ Files\\Scinomix\\SciPrint-VX\\Scinomix.Applications.SciPrintVX.exe";
    #const LCLAPPPATH   = 'start "" ' . '"C:\\Windows\\System32\\notepad.exe"';
    const LCLAPPPATH   = 'C:\\cli\\startExternal.bat';
    const LCLDSTFILE   = 'dstQuest.csv';
    const LCLSRCFILE   = 'srcQuest.csv';
    const LCLSTGFILE   = 'rack3ToDo.txt';

    # Member Variables
    private $dirHandle          = false;
    private $dryRun             = false;
    private $incomingPath       = false;
    private $incomingFiles      = [];
    private $incomingSourceFile = false;
    private $completePath       = false;
    private $procStart          = false;
    private $incomingDSTPath    = false;
    private $incomingSRCPath    = false;
    private $completeDSTPath    = false;
    private $completeSRCPath    = false;
    private $input              = false;
    private $output             = false;
    private $localDSTPath       = false;
    private $localSRCPath       = false;
    private $localDSTPathA      = false;
    private $localSRCPathA      = false;

    protected function configure()
    {   

        // Init
        //date_default_timezone_set('America/Los Angelas');
        $this->procStart     = date("D M j G:i:s T Y");
        $this->incomingPath  = self::UNCDRVL . self::UNCINDIR;
        $this->completePath  = self::UNCDRVL . self::UNCOUTDIR;

        $this->localDSTPath  = self::LCLINPATH . '\\' . self::LCLDSTFILE;
        $this->localSRCPath  = self::LCLINPATH . '\\' . self::LCLSRCFILE;
        $this->localDSTPathA = self::LCLINSTGPATH . '\\';
        $this->localSRCPathA = self::LCLINSTGPATH . '\\';

        // Startup
        $this->setName("Scinomix:doSCIVXLPrintSplit")
             ->setDescription("Copy and print to local Scinomix VXL System (Rack 1 & 2 only)")
             ->setDefinition(array(
                      new InputOption('dryrun', null, InputOption::VALUE_OPTIONAL, 'Dry run :: Will not execute printing'),
                ))
             ->setHelp('
1) Display appropriate files located in '.self::UNCPATH.'
2) User must choose one file (or to Exit) to print labels
3) The program will copy over the chosen file and trigger the Scinomix Printer

Usage:

<info>/c/php/php.exe console Scinomix:doSCIVXLPrintSplit</info>

You can also specify DRYRUN mode, which will work as normal but not actually
execute the file copies or program launch

<info>/c/php/php.exe console Scinomix:doSCIVXLPrintSplit --dryrun</info>
');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        // Setup
        $this->input  = $input;
        $this->output = $output;
        $this->dryRun = $input->hasParameterOption('--dryrun');
        
        // Start Pre-Process
        $this->doPreFlight();

        // Process
        $this->getValidFileListForSplitSet();

        $helper   = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Please select the incoming file to proceed (there is NO DEFAULT selection a choice is required)' . "\n",
            $this->incomingFiles
        );

        $question->setErrorMessage('File Selection %s is invalid, please try again:');
        $this->incomingSourceFile = $helper->ask($input, $output, $question);
        $output->writeln('You have just selected: ' . $this->incomingSourceFile);

        if($this->incomingSourceFile === self::CANCEL) {
            $this->doCancel();
        }

        if($this->dryRun === true) {
            $this->output->writeln("\n" . 'DRYRUN MODE ENGAGED, OPERATION COMPLETE'); 
            $this->doExit();
        } else {
            $this->doProcessFile();
        }
    }


    private function doPreFlight() 
    {

        $retVal = false;

        // Check mode
        $modeMessage = ($this->dryRun === true) ? 'ENABLED' : 'DISABLED (FILE PROCESSING *ACTIVE*)';
        $this->output->writeln("\n" . 'PROCESS START: ' . $this->procStart); 
        $this->output->writeln('DRYRUN MODE  : ' . $modeMessage . "\n");
        
        // Check paths
        $fs   = new Filesystem();

        // Local Input and Logging (ToDo)
        $test = $fs->exists(self::LCLINPATH);
        if($test === false) {
            throw new \Exception ("ERROR: Local Input Directory Does not exist: " . self::LCLINPATH);
        }

        // Local Input and Logging (ToDo)
        $test = $fs->exists(self::LCLINSTGPATH);
        if($test === false) {
            throw new \Exception ("ERROR: Local Input Staging Directory Does not exist: " . self::LCLINSTGPATH);
        }

        $test = $fs->exists(self::LCLLOGPATH);
        if($test === false) {
            throw new \Exception ("ERROR: Local Logging Directory Does not exist: " . self::LCLLOGPATH);
        }


        // Map the drive
        system("net use ".self::UNCDRVL.": \"".self::UNCPATH."\" ".self::UNCPASS." /user:".self::UNCUSER." /persistent:no>nul 2>&1");

        // Network Input & Output
        $test = $fs->exists($this->incomingPath);
        if($test === true) {
            // Verify that the dirctory is there
            if (is_dir($this->incomingPath)) {
                if ($this->dirHandle = opendir($this->incomingPath)) {
                    $retVal = true;
                } else {
                    // Exception
                }
            } else {
                // Exception
            }
        } else {
            throw new \Exception ("ERROR: Local Input Directory Does not exist: " . $this->incomingPath);
        }

        $test = $fs->exists($this->completePath);
        if($test === false) {
            throw new \Exception ("ERROR: Local Logging Directory Does not exist: " . $this->completePath);
        }

        return $retVal;
    }


    /**
     * There a total of 4 files per set, but we only show the user the one "set" file so we
     * need to filter the other 3 out.  Example of the "set" file:
     * NIPT17-XXXX-X_Set1.dst or NIPT17-XXXX-X_Set2.dst
     * So Each "Set" is:
     * NIPT17-XXXX-X_Set1.dst  --> Rack 1 & 2 Data (This is what we display for user choice)
     * NIPT17-XXXX-X_Set1.src  --> SRC Data for Rack 1 & 2 Data file (we copy)
     * NIPT17-XXXX-X_Set1a.dst --> Rack 3 Data (we copy)
     * NIPT17-XXXX-X_Set1a.src --> Rack 3 SRC Data (we copy)
     */
    private function getValidFileListForSplitSet() 
    {
        $files = array_diff(scandir($this->incomingPath), array('.', '..'));
        foreach($files as $file) {
            if(preg_match("/\ANIPT[1-9][0-9]-[0-9][0-9][0-9][0-9]-[0-9]\d*_Set[1-2]\.dst/", $file)){
                $this->incomingFiles[] = $file;
            }
        }
        array_push($this->incomingFiles, self::CANCEL);
        closedir($this->dirHandle);
        return $this->incomingFiles;
    }


    /**
     * There a total of 4 files per set, but we only show the user the one "set" file so we
     * need to filter the other 3 out.  Example of the "set" file:
     * NIPT17-XXXX-X_Set1.dst or NIPT17-XXXX-X_Set2.dst
     * So Each "Set" is:
     * NIPT17-XXXX-X_Set1.dst  --> Rack 1 & 2 Data (This is what we display for user choice)
     * NIPT17-XXXX-X_Set1.src  --> SRC Data for Rack 1 & 2 Data file (we copy)
     * NIPT17-XXXX-X_Set1a.dst --> Rack 3 Data (we copy)
     * NIPT17-XXXX-X_Set1a.src --> Rack 3 SRC Data (we copy)
     */
    private function doProcessFile() {
        
        //Build Full File Path(s)
        $basename = explode('.', $this->incomingSourceFile);

        // Rack 1 & 2
        $this->incomingDSTPath  = $this->incomingPath . '\\' . $this->incomingSourceFile;
        $this->incomingSRCPath  = $this->incomingPath . '\\' . $basename[0] . '.' . self::SRCEXT;
        
        $this->completeDSTPath  = $this->completePath . '\\' . $basename[0] . '.' . self::DSTEXT;
        $this->completeSRCPath  = $this->completePath . '\\' . $basename[0] . '.' . self::SRCEXT;


        // Rack 3
        $this->incomingDSTPathA = $this->incomingPath . '\\' . $basename[0] . 'a.' . self::DSTEXT;
        $this->incomingSRCPathA = $this->incomingPath . '\\' . $basename[0] . 'a.' . self::SRCEXT;
        
        $this->completeDSTPathA = $this->completePath . '\\' . $basename[0] . 'a.' . self::DSTEXT;
        $this->completeSRCPathA = $this->completePath . '\\' . $basename[0] . 'a.' . self::SRCEXT;


        // Check paths & Files
        $fs   = new Filesystem();

        // Check to see that these files have not been processed
        if($fs->exists($this->completeDSTPath) || $fs->exists($this->completeSRCPath)) {
            throw new \Exception ("ERROR: File(s) Already Processed: " . $this->completeDSTPath . ' or ' . $this->completeSRCPath);
        }

        // Verify full paths are correct then proceed
        $dstTest = $fs->exists($this->incomingDSTPath);
        if($dstTest === false) {
            throw new \Exception ("ERROR: No Incoming DST File: " . $this->incomingDSTPath);
        }

        $srcTest = $fs->exists($this->incomingSRCPath);
        if($srcTest === false) {
            throw new \Exception ("ERROR: No Incoming SRC File: " . $this->incomingSRCPath);
        }


        // Start Process
        $this->output->writeln("\n" . 'STARTING PROCESS FOR: ' . $this->incomingDSTPath);

        // Clear local input cache & Staging area
        $this->clearLocalInput($fs);

        // Copy Files Locally and rename
        $fs->copy($this->incomingDSTPath, $this->localDSTPath);
        $fs->copy($this->incomingSRCPath, $this->localSRCPath);
        $fs->copy($this->incomingDSTPathA, $this->localDSTPathA . $basename[0] . 'a.' . self::DSTEXT);
        $fs->copy($this->incomingSRCPathA, $this->localSRCPathA . $basename[0] . 'a.' . self::SRCEXT);

        // Copy on remote share (incoming to archive)
        $fs->copy($this->incomingDSTPath, $this->completeDSTPath);
        $fs->copy($this->incomingSRCPath, $this->completeSRCPath);
        $fs->copy($this->incomingDSTPathA, $this->completeDSTPathA);
        $fs->copy($this->incomingSRCPathA, $this->completeSRCPathA);

        // Remove source
        $fs->remove($this->incomingDSTPath);
        $fs->remove($this->incomingSRCPath);
        $fs->remove($this->incomingDSTPathA);
        $fs->remove($this->incomingSRCPathA);

        // Write Local Staging File
        file_put_contents(self::LCLINSTGPATH . '\\' . self::LCLSTGFILE, $basename[0] . 'a.' . self::DSTEXT);

        // Launch Instrument Application
        $this->launchLocalApp();
        $this->doExit();

    }


    private function clearLocalInput($fs) {

        // Process File Deletions
        // Local Inputs have specific file names
        if($fs->exists($this->localDSTPath)) {
            $fs->remove($this->localDSTPath);
        }

        if($fs->exists($this->localSRCPath)) {
            $fs->remove($this->localSRCPath);
        }

        $files = glob(self::LCLINSTGPATH . '\\*'); //get all file names
        foreach($files as $file){
            if(is_file($file)) {
                unlink($file);
            }
        }


        // Verify File Deletions (counts should both be zero)
        $countInputs = $countStaged = 0;
        $files = glob(self::LCLINSTGPATH . '\\*'); //get all file names
        foreach($files as $file){
            $countStaged++;
        }        

        $files = glob(self::LCLINPATH . '\\*'); //get all file names
        foreach($files as $file){
            $countInputs++;
        } 


        if($countInputs > 0 || $countStaged > 0) {
            throw new \Exception ("ERROR: FAILURE TO REMOVE ALL LOCAL FILES BEFORE PROCESSING!");
        }
        return true;
    }


    private function launchLocalApp() {
        // Launch Local Application

        $process = new Process(self::LCLAPPPATH);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $this->output->writeln('PROCESS COMPLETE: ' . $process->getOutput());

    }


    private function doCancel() {
        $this->output->writeln("\n" . 'OPERATION CANCELED' . "\n");
        $this->doExit();
    }


    private function doExit() {
        exit();
    }

}