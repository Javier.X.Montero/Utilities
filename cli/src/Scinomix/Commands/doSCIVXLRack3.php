<?php

namespace Scinomix\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\ProcessBuilder;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class doSCIVXLRack3 extends Command {

    # Member Constants
    const LCLPATH      = 'C:\\cli';
    const LCLINPATH    = 'C:\\Batch Labeller\\WorkTODO';
    const LCLINSTGPATH = 'C:\\cli\\inputStaging';
    const LCLLOGPATH   = 'C:\\cli\\log';

    const DSTEXT       = 'dst';
    const SRCEXT       = 'src';
    const CANCEL       = '***CANCEL/ABORT***';
    const LCLAPPPATH   = 'start "" ' . '"C:\\Windows\\System32\\notepad.exe"';
    const LCLDSTFILE   = 'dstQuest.csv';
    const LCLSRCFILE   = 'srcQuest.csv';
    const LCLSTGFILE   = 'rack3ToDo.txt';

    # Member Variables
    private $dirHandle          = false;
    private $dryRun             = false;
    private $procStart          = false;
    private $input              = false;
    private $output             = false;
    private $incomingFiles      = [];       // Incoming file for Rack 3 processing, should only be one file
    private $localDSTPath       = false;    // Abs path for input DST file including filename (configured)
    private $localSRCPath       = false;    // Abs path for input SRC file including filename (configured)
    private $localDSTPathA      = false;    // Abs path for the source DST file to copy (read from staging)
    private $localSRCPathA      = false;    // Abs path for the source SRC file to copy (read from staging)
    private $localSTGPath       = false;    // Abs path for the file with the target name

    protected function configure()
    {   

        // Init
        //date_default_timezone_set('America/Los Angelas');
        $this->procStart     = date("D M j G:i:s T Y");

        $this->localDSTPath  = self::LCLINPATH . '\\' . self::LCLDSTFILE;
        $this->localSRCPath  = self::LCLINPATH . '\\' . self::LCLSRCFILE;
        $this->localSTGPath  = self::LCLINSTGPATH . '\\' . self::LCLSTGFILE;

        // Startup
        $this->setName("Scinomix:doSCIVXLRack3")
             ->setDescription("Copy and Print Labels for Rack 3")
             ->setDefinition(array(
                      new InputOption('dryrun', null, InputOption::VALUE_OPTIONAL, 'Dry run :: Will not execute printing'),
                ))
             ->setHelp('
1) Clear the local working input area: 
2) Check to make sure that only two files are in staging (one dst and one src)
3) Copy the staging files over to the local working area 
4) Clear out the local staging area

Usage:

<info>/c/php/php.exe console Scinomix:doSCIVXLRack3</info>

You can also specify DRYRUN mode, which will work as normal but not actually
execute the file copies or program launch

<info>/c/php/php.exe console Scinomix:doSCIVXLRack3 --dryrun</info>
');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        // Setup
        $this->input  = $input;
        $this->output = $output;
        $this->dryRun = $input->hasParameterOption('--dryrun');
        
        // Do we even bother....
        // Verify File Deletions (counts should both be zero)
        $countInputs = 0;
        $files = glob(self::LCLINSTGPATH . '\\*'); //get all file names
        foreach($files as $file){
            $countInputs++;
        }     

        if($countInputs === 0) {
            $this->output->writeln("\n" . 'NO INPUTS FOUND, EXITING'); 
            $this->doExit();  
        }   


        // Start Pre-Process
        $this->doPreFlight();

        // Process
        $this->getrack3FileForPrint();

        if($this->dryRun === true) {
            $this->output->writeln("\n" . 'DRYRUN MODE ENGAGED, OPERATION COMPLETE'); 
            $this->doExit();
        } else {
            $this->doProcessFile();
        }
    }


    private function doPreFlight() 
    {

        $retVal = false;

        // Check mode
        $modeMessage = ($this->dryRun === true) ? 'ENABLED' : 'DISABLED (FILE PROCESSING *ACTIVE*)';
        $this->output->writeln("\n" . 'PROCESS START: ' . $this->procStart); 
        $this->output->writeln('DRYRUN MODE  : ' . $modeMessage . "\n");
        
        // Check paths
        $fs   = new Filesystem();

        // Local Input and Logging (ToDo)
        $test = $fs->exists(self::LCLINPATH);
        if($test === false) {
            throw new \Exception ("ERROR: Local Input Directory Does not exist: " . self::LCLINPATH);
        }

        // Local Input and Logging (ToDo)
        $test = $fs->exists(self::LCLINSTGPATH);
        if($test === false) {
            throw new \Exception ("ERROR: Local Input Staging Directory Does not exist: " . self::LCLINSTGPATH);
        }

        $test = $fs->exists(self::LCLLOGPATH);
        if($test === false) {
            throw new \Exception ("ERROR: Local Logging Directory Does not exist: " . self::LCLLOGPATH);
        }

        $test = $fs->exists($this->localSTGPath);
        if($test === false) {
            throw new \Exception ("ERROR: Local Rack3 Queue Staging File Does Not Exist: " . $this->localSTGPath);
        }

        return $retVal;
    }


    /**
     * NIPT17-XXXX-X_Set1a.dst --> Rack 3 Data (we copy)
     * NIPT17-XXXX-X_Set1a.src --> Rack 3 SRC Data (we copy)
     */
    private function getrack3FileForPrint() 
    {
        
        // The previous process wrote the name of the target file into a file, let's get it
        $inputDSTTargetFileName = file_get_contents($this->localSTGPath);

        $e = explode('.', $inputDSTTargetFileName);
        $inputSRCTargetFileName =$e[0] . '.' . self::SRCEXT;

        $this->incomingDSTPathA = self::LCLINSTGPATH . '\\' . $inputDSTTargetFileName;
        $this->incomingSRCPathA = self::LCLINSTGPATH . '\\' . $inputSRCTargetFileName;

        // Verify the file(s) exist in the correct path
        $fs   = new Filesystem();
        $test = $fs->exists($this->incomingDSTPathA);
        if($test === false) {
            throw new \Exception ("ERROR: Local Rack3 DST File Does Not Exist: " . $this->incomingDSTPathA);
        }

        $test = $fs->exists($this->incomingSRCPathA);
        if($test === false) {
            throw new \Exception ("ERROR: Local Rack3 SRC File Does Not Exist: " . $this->incomingSRCPathA);
        }

        return $this->incomingFiles;
    }


    private function doProcessFile() {
        
        $fs   = new Filesystem();

        // Start Process
        $this->output->writeln("\n" . 'STARTING PROCESS FOR: ' . $this->incomingDSTPathA);

        // Clear local input cache & Staging area
        $this->clearLocalInput($fs);

        // Copy Files Locally and rename
        $fs->copy($this->incomingDSTPathA, $this->localDSTPath);
        $fs->copy($this->incomingSRCPathA, $this->localSRCPath);

        // Clear local staging
        $this->clearLocalStaging();
        $this->output->writeln("\n" . 'OPERATION COMPLETE');
        $this->doExit();
    }

    private function clearLocalInput($fs) {

        // Local Input has specific file names
        if($fs->exists($this->localDSTPath)) {
            $fs->remove($this->localDSTPath);
        }

        if($fs->exists($this->localSRCPath)) {
            $fs->remove($this->localSRCPath);
        }

        return true;
    }


    private function clearLocalStaging() {

        $files = glob(self::LCLINSTGPATH . '\\*'); //get all file names
        foreach($files as $file){
            if(is_file($file)) {
                unlink($file);
            }
        }

    }


    private function launchLocalApp() {
        // Launch Local Application

        $process = new Process(self::LCLAPPPATH);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $this->output->writeln('PROCESS COMPLETE: ' . $process->getOutput());

    }


    private function doCancel() {
        $this->output->writeln("\n" . 'OPERATION CANCELED' . "\n");
        $this->doExit();
    }


    private function doExit() {
        exit();
    }

}